(function( $ ){

	var documentHeight = $('header').height();
	var mainHeight = $('#main').height();

	$(window).bind('scroll',function(e){
	    parallaxScroll();
	});

	var section1_start = parseInt($('.section1').css('top'));
	var section2_start = parseInt($('.section2').css('top'));
	var section3_start = parseInt($('.section3').css('top'));
	var section4_start = parseInt($('.section4').css('top'));
	var section5_start = parseInt($('.section5').css('top'));
	var section6_start = parseInt($('.section6').css('top'));
	var section7_start = parseInt($('.section7').css('top'));

	function parallaxScroll(){
	    var scrolled = $(window).scrollTop();

	    if( scrolled > documentHeight ) {
	    	scrolled = scrolled - documentHeight;
		    $('.section1').css('top', section1_start + (0-(scrolled*.2))+'px');
		    $('.section2').css('top', section2_start + (0-(scrolled*.5))+'px');
		    $('.section3').css('top', section3_start + (0-(scrolled*.8))+'px');
		    $('.section4').css('top', section4_start + (0-(scrolled*1.1))+'px');
		    $('.section5').css('top', section5_start + (0-(scrolled*1.43))+'px');
		    $('.section6').css('top', section6_start + (0-(scrolled*1.9))+'px');
		    $('.section7').css('top', section7_start + (0-(scrolled*2.5))+'px');

		    $('#fixed-content-panel').show();
		}
		else {
			$('#fixed-content-panel').hide();
		}
	    //console.log( section1_start + (0-(scrolled*.1))+'px' );
	}

	$(function(){
		$('#contact').css('top', (documentHeight * 2 * 0.9) + mainHeight + 'px');

		var $window = $(window);
		var scrollTime = .5;
		var scrollDistance = 90;

		$window.on("mousewheel DOMMouseScroll", function(event){

			event.preventDefault();

			var delta = event.originalEvent.wheelDelta/120 || -event.originalEvent.detail/3;
			var scrollTop = $window.scrollTop();
			var finalScroll = scrollTop - parseInt(delta*scrollDistance);

			TweenMax.to($window, scrollTime, {
			scrollTo : { y: finalScroll, autoKill:true },
				ease: Power1.easeOut,
				overwrite: 5
			});

		});

	});

	// SMOOTH SCROLL
	$('a[href*=#]:not([href=#])').click(function() {
        if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
            var target = $(this.hash);
            target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
            if (target.length) {
                $('html,body').animate({
                    scrollTop: target.offset().top
                }, 2500);
                return false;
            }
        }
    });
	// END: SMOOTH SCROLL


	$(document).ready(function(){

		var breakPoint = $('#break-point').width();
		console.log(breakPoint);

		if(breakPoint >= 992) {
			setTimeout(function() {
				$('#loader').fadeOut(600);
			}, 1000);
		} else {
			$('#loader').hide();
		}
	});

	$(window).resize(function(){
		var breakPoint = $('#break-point').width();
		if(breakPoint < 992) {
			$('#loader').hide();
		}
	});


	$('form').submit(function(){

        var valid = true;

        $('.error-message').hide();
        $('.submit-error').removeClass('on');
        $('input').removeClass('input-error');
        $('select').removeClass('input-error-select');


        if( ! $('#firstname').val() ) {
            $('#firstname').addClass('input-error');
            valid = false;
        }
        if( ! $('#lastname').val() ) {
            $('#lastname').addClass('input-error');
            valid = false;
        }
        if( ! $('#phonenumber').val() ) {
            $('#phonenumber').addClass('input-error');
            valid = false;
        }
        if( ! $('#email').val() ) {
            $('#email').addClass('input-error');
            valid = false;
        }
        if( ! $('#q1').val() ) {
            $('#q1').addClass('input-error-select');
            valid = false;
        }


        if( ! valid ) {
            $('.submit-error').addClass('on');
            return false;
        }
        else { return true; }

    });



})( jQuery );
